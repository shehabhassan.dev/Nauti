<!DOCTYPE html>
<html lang="en" class="light scroll-smooth" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <title>NAUTI</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Tailwind CSS Saas & Software Landing Page Template">
        <meta name="keywords" content="agency, application, business, clean, creative, cryptocurrency, it solutions, modern, multipurpose, nft marketplace, portfolio, saas, software, tailwind css">
        <meta name="author" content="Shreethemes">
        <meta name="website" content="https://shreethemes.in">
        <meta name="email" content="support@shreethemes.in">
        <meta name="version" content="2.1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <link rel="shortcut icon" href="assets/images/Images/logo.png">

        <!-- Css -->
        <link href="assets/libs/tiny-slider/tiny-slider.css" rel="stylesheet">
        <link href="assets/libs/tobii/css/tobii.min.css" rel="stylesheet">
        <!-- Main Css -->
        <link href="assets/libs/@iconscout/unicons/css/line.css" type="text/css" rel="stylesheet">
        <link href="assets/libs/@mdi/font/css/materialdesignicons.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="assets/css/tailwind.css">

    </head>
    
    <body class="font-nunito text-base text-black dark:text-white dark:bg-slate-900">
        <!-- Loader Start -->
        <!-- <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div> -->
        <!-- Loader End -->

        
        <!-- Start Navbar -->
        <nav id="topnav" class="defaultscroll is-sticky">
            <div class="container relative">
                <!-- Logo container-->
                <a class="logo my-2" href="Home-en.html">
                    <span class="inline-block dark:hidden">
                        <img src="assets/images/Images/logo.png" class="l-dark h-12" height="24" alt="">
                        <img src="assets/images/Images/logo.png" class="l-light h-16" height="24" alt="">
                    </span>
                    <img src="assets/images/Images/logo.png" height="24" class="hidden dark:inline-block" alt="">
                </a>

                <!-- End Logo container-->
                <div class="menu-extras">
                    <div class="menu-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle" id="isToggle" onclick="toggleMenu()">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </div>
                </div>

                <!--Login button Start-->
                <ul class="buy-button list-none mb-0">
                    <li class="inline mb-0">
                        <a href="">
                            <span class="login-btn-primary"><span class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center rounded-full bg-indigo-600/5 hover:bg-indigo-600 border border-indigo-600/10 hover:border-indigo-600 text-indigo-600 hover:text-white"><i data-feather="shopping-cart" class="h-4 w-4"></i></span></span>
                            <span class="login-btn-light"><span class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center rounded-full bg-gray-50 hover:bg-gray-200 dark:bg-slate-900 dark:hover:bg-gray-700 border hover:border-gray-100 dark:border-gray-700 dark:hover:border-gray-700"><i data-feather="shopping-cart" class="h-4 w-4"></i></span></span>
                        </a>
                    </li>
            

                </ul>
                <!--Login button End-->

                <div id="navigation">
                    <!-- Navigation Menu-->   
                    <ul class="navigation-menu nav-light">
                        <li><a href="Home-en.html" class="sub-menu-item">Home</a></li>
                        <li><a href="About-en.html" class="sub-menu-item">About</a></li>
                        <li><a href="Activities-en.html" class="sub-menu-item">Activities</a></li>
                        <li><a href="Videos-en.html" class="sub-menu-item">Videos</a></li>
                        <li><a href="ENVIRONMENTAL-en.html" class="sub-menu-item">ENVIRONMENTAL PLEDGE</a></li>


                        <li><a href="Contact-en.html" class="sub-menu-item">Contact</a></li>
                    </ul><!--end navigation menu-->
                </div><!--end navigation-->
            </div><!--end container-->
        </nav><!--end header-->
        <!-- End Navbar -->

        <!-- Start Hero -->
        <section id="controls-carousel" class="relative" data-carousel="static">
            <div class="overflow-hidden relative h-screen inset-0">


                <div class="flex items-center justify-center duration-700 ease-in-out" data-carousel-item="">
                    <div class="absolute inset-0 image-wrap z-1 bg-top bg-no-repeat bg-cover" style="background-image: url(../dist/assets/images/Images/bg3.jpg);"></div>
                    <div class="absolute inset-0 ltr:md:bg-gradient-to-l rtl:md:bg-gradient-to-r md:from-black md:via-black/80 md:bg-black/20 bg-black/70 z-2"></div>
                    <div class="container relative z-3">
                        <div class="grid grid-cols-1 mt-10">
                            <div class="md:text-end text-center">
                                <h1 class="font-bold text-white lg:leading-normal leading-normal text-4xl lg:text-5xl mb-6">Build and Grow Your Business</h1>
                                <p class="text-white/70 text-lg max-w-xl md:ms-auto">Launch your campaign and benefit from our expertise on designing and managing conversion centered Tailwind CSS v3.x html page.</p>
                                
                                <div class="mt-8">
                                    <a href="" class="py-2 px-5 inline-block font-semibold tracking-wide border align-middle duration-500 text-base text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-md">Pricing Plans</a>
                                </div>
                            </div>
                        </div><!--end grid-->
                    </div><!--end container-->
                </div>

                <div class="flex items-center justify-center duration-700 ease-in-out" data-carousel-item="">
                    <div class="absolute inset-0 image-wrap z-1  bg-top bg-no-repeat bg-cover" style="background-image: url(../dist/assets/images/Images/bg1.jpg);"></div>
                    <div class="absolute inset-0 md:bg-gradient-to-b md:from-transparent md:to-black md:bg-black/20 bg-black/70 z-2"></div>
                    <div class="container relative z-3">
                        <div class="grid grid-cols-1 mt-10">
                            <div class="text-center">
                                <h1 class="font-bold text-white lg:leading-normal leading-normal text-4xl lg:text-5xl mb-6">Let's Start With Techwind</h1>
                                <p class="text-white/70 text-lg max-w-xl mx-auto">Launch your campaign and benefit from our expertise on designing and managing conversion centered Tailwind CSS v3.x html page.</p>
                                
                                <div class="mt-8">
                                    <a href="" class="py-2 px-5 inline-block font-semibold tracking-wide border align-middle duration-500 text-base text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-md">See Services</a>
                                </div>
                            </div>
                        </div><!--end grid-->
                    </div><!--end container-->
                </div>

                <div class="flex items-center justify-center duration-700 ease-in-out" data-carousel-item="">
                    <div class="absolute inset-0 image-wrap z-1 bg-top bg-no-repeat bg-cover" style="background-image: url(../dist/assets/images/Images/bg2.jpg);"></div>
                    <div class="absolute inset-0 ltr:md:bg-gradient-to-l rtl:md:bg-gradient-to-r md:from-black md:via-black/80 md:bg-black/20 bg-black/70 z-2"></div>
                    <div class="container relative z-3">
                        <div class="grid grid-cols-1 mt-10">
                            <div class="md:text-end text-center">
                                <h1 class="font-bold text-white lg:leading-normal leading-normal text-4xl lg:text-5xl mb-6">Build and Grow Your Business</h1>
                                <p class="text-white/70 text-lg max-w-xl md:ms-auto">Launch your campaign and benefit from our expertise on designing and managing conversion centered Tailwind CSS v3.x html page.</p>
                                
                                <div class="mt-8">
                                    <a href="" class="py-2 px-5 inline-block font-semibold tracking-wide border align-middle duration-500 text-base text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-md">Pricing Plans</a>
                                </div>
                            </div>
                        </div><!--end grid-->
                    </div><!--end container-->
                </div>
            </div>

            <button type="button" class="flex absolute top-0 start-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none" data-carousel-prev="">
                <span class="inline-flex justify-center items-center w-8 h-8 rounded-full border border-white hover:border-indigo-600 hover:bg-indigo-600 group-focus:outline-none">
                    <svg class="w-4 h-4 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path>
                    </svg>
                    <span class="hidden">Previous</span>
                </span>
            </button>
            <button type="button" class="flex absolute top-0 end-0 z-30 justify-center items-center px-4 h-full cursor-pointer group focus:outline-none" data-carousel-next="">
                <span class="inline-flex justify-center items-center w-8 h-8 rounded-full border border-white hover:border-indigo-600 hover:bg-indigo-600 group-focus:outline-none">
                    <svg class="w-4 h-4 text-white" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path>
                    </svg>
                    <span class="hidden">Next</span>
                </span>
            </button>
            
        </section><!--end section-->
        <!-- End Hero -->

        <!-- End Hero -->

        <section class="relative md:py-24 py-16 overflow-hidden">
            <div class="container relative">
                <div class="grid md:grid-cols-12 grid-cols-1 items-center gap-[30px]">
                    <div class="lg:col-span-5 md:col-span-6">
                        <div class="relative">
                            <img src="assets/images/Images/about1.jpg" class="rounded-full lg:w-[400px] h-[500px] w-[280px]" alt="">
                            <div class="absolute -end-5 -bottom-16">
                                <img src="assets/images/Images/about2.jpg" class="rounded-full lg:w-[280px] w-[200px] border-8 border-white dark:border-slate-900" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="lg:col-span-7 md:col-span-6 mt-8 md:mt-0">
                        <div class="lg:ms-5">
                            <h6 class="text-indigo-600 text-sm font-bold uppercase mb-2">NAUTI</h6>
                            <h3 class="mb-4 md:text-3xl md:leading-normal text-2xl leading-normal font-semibold">Abbreviation of Nautical: of, relating to, or associated with seamen, navigation, or ships.</h3>
                            <p class="text-slate-400 max-w-xl">Start working with Tailwind CSS that can provide everything you need to generate awareness, drive traffic, connect. Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with 'real' content.</p>

                            <ul class="list-none text-slate-400 mt-4">
                                <li class="mb-1 flex"><i class="uil uil-check-circle text-indigo-600 text-xl me-2"></i>Lorem ipsum dolor sit amet consectetur.</li>
                                <li class="mb-1 flex"><i class="uil uil-check-circle text-indigo-600 text-xl me-2"></i> Lorem ipsum dolor sit amet consectetur.</li>
                                <li class="mb-1 flex"><i class="uil uil-check-circle text-indigo-600 text-xl me-2"></i> Create your own skin to match your brand</li>
                            </ul>
                        
                            <div class="mt-6">
                                <a href="about.html" class="py-2 px-5 inline-block font-semibold tracking-wide border align-middle duration-500 text-base text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-md me-2 mt-2"><i class="uil uil-keyboard-show"></i> Know More</a>
                            </div>
                        </div>
                    </div>
                </div><!--end grid-->
            </div><!--end container-->




            <section class="relative  py-16" id="portfolio">
  
            <div class="container relative md:mt-24 mt-16">

                <div class="container relative ">
                    <div class="grid grid-cols-1 pb-8 items-end">
                        <h3 class="mb-4 md:text-3xl md:leading-normal text-2xl leading-normal font-semibold text-center">How we work</h3>
                        <p class="text-slate-400 text-center">Integrated marine experiances...for all !</p>
                    </div><!--end grid-->
                </div><!--end container-->
                <div class="grid lg:grid-cols-12 md:grid-cols-2 grid-cols-1 items-center mt-10 gap-[30px]">
                    <div class="lg:col-span-6">

                        <div class="shadow-md dark:shadow-gray-800  border-8 border-gray-200 dark:border-gray-900">
                            <img src="assets/images/Images/faq.jpg" class="shadow-md  w-full h-[600px]" alt="">
                        </div>
                    </div>
                    <div class="lg:col-span-6">
                        <div class="grid grid-cols-1 gap-[30px]">
                            <div class="group flex items-center relative overflow-hidden p-6 rounded-md shadow dark:shadow-gray-800 bg-gray-50 dark:bg-slate-800 hover:bg-indigo-600 dark:hover:bg-indigo-600 duration-500">
                                <span class="text-indigo-600 group-hover:text-white text-5xl font-semibold duration-500">
                                    <i class="uil uil-swatchbook"></i>
                                </span>
                                <div class="flex-1 ms-3">
                                    <h5 class="group-hover:text-white text-xl font-semibold duration-500">Beach Clubs - Shoreside and Floating                                    </h5>
                                    <p class="text-slate-400 group-hover:text-white/50 duration-500 mt-2">There are many variations of passages of Lorem Ipsum available</p>
                                </div>
                                <div class="absolute start-1 top-5 text-dark/[0.03] dark:text-white/[0.03] text-8xl group-hover:text-white/[0.04] duration-500">
                                    <i class="uil uil-swatchbook"></i>
                                </div>
                            </div>
    
                            <div class="group flex items-center relative overflow-hidden p-6 rounded-md shadow dark:shadow-gray-800 bg-gray-50 dark:bg-slate-800 hover:bg-indigo-600 dark:hover:bg-indigo-600 duration-500">
                                <span class="text-indigo-600 group-hover:text-white text-5xl font-semibold duration-500">
                                    <i class="uil uil-tachometer-fast-alt"></i>
                                </span>
                                <div class="flex-1 ms-3">
                                    <h5 class="group-hover:text-white text-xl font-semibold duration-500">Vessels - Pontoons, Boats, Jet skis, etc.                                    </h5>
                                    <p class="text-slate-400 group-hover:text-white/50 duration-500 mt-2">There are many variations of passages of Lorem Ipsum available</p>
                                </div>
                                <div class="absolute start-1 top-5 text-dark/[0.03] dark:text-white/[0.03] text-8xl group-hover:text-white/[0.04] duration-500">
                                    <i class="uil uil-tachometer-fast-alt"></i>
                                </div>
                            </div>
    
                            <div class="group flex items-center relative overflow-hidden p-6 rounded-md shadow dark:shadow-gray-800 bg-gray-50 dark:bg-slate-800 hover:bg-indigo-600 dark:hover:bg-indigo-600 duration-500">
                                <span class="text-indigo-600 group-hover:text-white text-5xl font-semibold duration-500">
                                    <i class="uil uil-user-check"></i>
                                </span>
                                <div class="flex-1 ms-3">
                                    <h5 class="group-hover:text-white text-xl font-semibold duration-500">Retail - Branded water toys, beach accessories, ice boxes, etc.</h5>
                                    <p class="text-slate-400 group-hover:text-white/50 duration-500 mt-2">There are many variations of passages of Lorem Ipsum available</p>
                                </div>
                                <div class="absolute start-1 top-5 text-dark/[0.03] dark:text-white/[0.03] text-8xl group-hover:text-white/[0.04] duration-500">
                                    <i class="uil uil-user-check"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--end grid-->
            </div><!--end container-->
            </section>

        </section><!--end section-->
        <!-- end section -->
        <section class="relative " id="portfolio">
            <div class="container relative ">
                <div class="grid grid-cols-1 pb-8 items-end">
                    <h3 class="mb-4 md:text-3xl md:leading-normal text-2xl leading-normal font-semibold">Our Products</h3>
                    <p class="text-slate-400 max-w-xl">Explore and learn more about everything from machine learning and global payments to scaling your team.</p>
                </div><!--end grid-->
            </div><!--end container-->

            <div class="container-fluid relative mt-8">
                <div class="grid grid-cols-1 mt-8">
                    <div class="tiny-six-item">
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p1.jpg" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p1.jpg" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p2.jpg" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p2.jpg" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p3.jpg" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p3.jpg" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p4.png" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p4.png" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p5.jpg" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p5.jpg" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p6.jpg" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p6.jpg" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p7.jpg" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p7.jpg" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="tiny-slide">
                            <div class="group relative block overflow-hidden rounded-md duration-500 mx-2">
                                <a href="assets/images/Images/p8.jpg" class="lightbox duration-500 group-hover:scale-105 h-80 w-full" title="">
                                    <img src="assets/images/Images/p8.jpg" class="h-80 w-full" alt="work-image">
                                </a>
                                <div class="absolute -bottom-52 group-hover:bottom-2 start-2 end-2 duration-500 bg-white dark:bg-slate-900 p-4 rounded shadow dark:shadow-gray-800">
                                    <a href="portfolio-detail-two.html" class="hover:text-indigo-600 text-lg duration-500 font-medium">Iphone mockup</a>
                                    <h6 class="text-slate-400">Branding</h6>
                                </div>
                            </div>
                        </div>
                        
                       
                        

                    </div>
                </div>
            </div><!--end container-->
        </section>

        <section class="relative md:py-24 py-16" id="portfolio">

            
            <div class="container relative">
                <div class="grid grid-cols-1 pb-8 text-center">
                    <h3 class="mb-4 md:text-3xl md:leading-normal text-2xl leading-normal font-semibold">OUR GALLERY                    </h3>

                    <p class="text-slate-400 max-w-xl mx-auto">Obviously I'm a Web Designer. Experienced with all stages of the development cycle for dynamic web projects.</p>
                </div><!--end grid-->

                <div class="grid grid-cols-1 lg:grid-cols-4 md:grid-cols-2 gap-6 mt-8">
                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g1.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g1.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>

                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g2.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g2.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>

                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g3.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g3.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>

                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g4.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g4.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>

                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g5.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g5.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>

                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g6.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g6.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>

                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g7.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g7.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>

                    <div class="group relative block overflow-hidden rounded-md duration-500">
                        <img src="assets/images/Images/g8.jpg" class="group-hover:origin-center group-hover:scale-110 group-hover:rotate-3 duration-500  h-80 w-full" alt="">
                        <div class="absolute inset-0 group-hover:bg-dark opacity-50 duration-500 z-0"></div>

                        <div class="content">
                            <div class="icon absolute z-10 opacity-0 group-hover:opacity-100 top-4 end-4 duration-500">
                                <a href="assets/images/Images/g8.jpg" class="h-9 w-9 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-full lightbox"><i class="uil uil-camera"></i></a>
                            </div>

                            <div class="title absolute z-10 opacity-0 group-hover:opacity-100 bottom-4 start-4 duration-500">
                                <a href="" class="h6 text-md font-medium text-white hover:text-indigo-600 duration-500 ease-in-out">Mockup Collection</a>
                                <p class="text-slate-100 tag mb-0">Abstract</p>
                            </div>
                        </div>
                    </div>
                </div><!--end grid-->
            </div><!--end container-->


            
            <div class="container relative md:mt-24 mt-16">
                <div class="grid grid-cols-1 pb-8 text-center">
                    <h3 class="mb-4 md:text-3xl md:leading-normal text-2xl leading-normal font-semibold">Frequently Asked Questions</h3>

                    <p class="text-slate-400 max-w-xl mx-auto">Start working with Tailwind CSS that can provide everything you need to generate awareness, drive traffic, connect.</p>
                </div><!--end grid-->

                <div class="relative grid md:grid-cols-12 grid-cols-1 items-center mt-8 gap-[30px]">
                    <div class="md:col-span-6">
                        <img src="assets/images/Images/faq.jpg" class="dark:shadow-gray-800 h-[600px] rounded-md shadow w-full" alt="">
                    </div><!--end col-->

                    <div class="md:col-span-6">
                        <div id="accordion-collapse" data-accordion="collapse">
                            <div class="relative shadow dark:shadow-gray-800 rounded-md overflow-hidden">
                                <h2 class="text-base font-semibold" id="accordion-collapse-heading-1">
                                    <button type="button" class="flex justify-between items-center p-5 w-full font-medium text-start" data-accordion-target="#accordion-collapse-body-1" aria-expanded="true" aria-controls="accordion-collapse-body-1">
                                        <span>How does it work ?</span>
                                        <svg data-accordion-icon class="w-4 h-4 rotate-180 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </button>
                                </h2>
                                <div id="accordion-collapse-body-1" class="hidden" aria-labelledby="accordion-collapse-heading-1">
                                    <div class="p-5">
                                        <p class="text-slate-400 dark:text-gray-400">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                    </div>
                                </div>
                            </div>
    
                            <div class="relative shadow dark:shadow-gray-800 rounded-md overflow-hidden mt-4">
                                <h2 class="text-base font-semibold" id="accordion-collapse-heading-2">
                                    <button type="button" class="flex justify-between items-center p-5 w-full font-medium text-start" data-accordion-target="#accordion-collapse-body-2" aria-expanded="false" aria-controls="accordion-collapse-body-2">
                                        <span>Do I need a designer to use Techwind ?</span>
                                        <svg data-accordion-icon class="w-4 h-4 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </button>
                                </h2>
                                <div id="accordion-collapse-body-2" class="hidden" aria-labelledby="accordion-collapse-heading-2">
                                    <div class="p-5">
                                        <p class="text-slate-400 dark:text-gray-400">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                    </div>
                                </div>
                            </div>
    
                            <div class="relative shadow dark:shadow-gray-800 rounded-md overflow-hidden mt-4">
                                <h2 class="text-base font-semibold" id="accordion-collapse-heading-3">
                                    <button type="button" class="flex justify-between items-center p-5 w-full font-medium text-start" data-accordion-target="#accordion-collapse-body-3" aria-expanded="false" aria-controls="accordion-collapse-body-3">
                                        <span>What do I need to do to start selling ?</span>
                                        <svg data-accordion-icon class="w-4 h-4 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </button>
                                </h2>
                                <div id="accordion-collapse-body-3" class="hidden" aria-labelledby="accordion-collapse-heading-3">
                                    <div class="p-5">
                                        <p class="text-slate-400 dark:text-gray-400">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="relative shadow dark:shadow-gray-800 rounded-md overflow-hidden mt-4">
                                <h2 class="text-base font-semibold" id="accordion-collapse-heading-4">
                                    <button type="button" class="flex justify-between items-center p-5 w-full font-medium text-start" data-accordion-target="#accordion-collapse-body-4" aria-expanded="false" aria-controls="accordion-collapse-body-4">
                                        <span>What happens when I receive an order ?</span>
                                        <svg data-accordion-icon class="w-4 h-4 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </button>
                                </h2>
                                <div id="accordion-collapse-body-4" class="hidden" aria-labelledby="accordion-collapse-heading-4">
                                    <div class="p-5">
                                        <p class="text-slate-400 dark:text-gray-400">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="relative shadow dark:shadow-gray-800 rounded-md overflow-hidden mt-4">
                                <h2 class="text-base font-semibold" id="accordion-collapse-heading-5">
                                    <button type="button" class="flex justify-between items-center p-5 w-full font-medium text-start" data-accordion-target="#accordion-collapse-body-5" aria-expanded="false" aria-controls="accordion-collapse-body-5">
                                        <span>How does it work ?</span>
                                        <svg data-accordion-icon class="w-4 h-4 rotate-180 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                                        </svg>
                                    </button>
                                </h2>
                                <div id="accordion-collapse-body-5" class="hidden" aria-labelledby="accordion-collapse-heading-5">
                                    <div class="p-5">
                                        <p class="text-slate-400 dark:text-gray-400">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--end col-->
                </div><!--end grid-->
            </div><!--end container-->

            <div class="container relative mt-16">
                <div class="relative bg-white dark:bg-slate-900 lg:px-8 px-6 py-10 rounded-xl shadow dark:shadow-gray-800 overflow-hidden">
                    <div class="grid md:grid-cols-2 grid-cols-1 items-center gap-[30px]">
                        <div class="md:text-start text-center z-1">
                            <h3 class="md:text-3xl text-2xl md:leading-normal leading-normal font-semibold">Subscribe to Newsletter!</h3>
                            <p class="text-slate-400 max-w-xl mx-auto mt-2">Subscribe to get latest updates and information.</p>
                        </div>

                        <div class="subcribe-form z-1">
                            <form class="relative max-w-xl">
                                <input type="email" id="subcribe" name="email" class="py-4 pe-40 ps-6 w-full h-[50px] outline-none text-black dark:text-white rounded-full bg-white dark:bg-slate-900 shadow dark:shadow-gray-800" placeholder="Your Email Address :">
                                <button type="submit" class="py-2 px-5 inline-block font-semibold tracking-wide align-middle duration-500 text-base text-center absolute top-[2px] end-[3px] h-[46px] bg-indigo-600 hover:bg-indigo-700 border border-indigo-600 hover:border-indigo-700 text-white rounded-full">Subscribe</button>
                            </form><!--end form-->
                        </div>
                    </div>

                    <div class="absolute -top-5 -start-5">
                        <div class="uil uil-envelope lg:text-[150px] text-7xl text-slate-900/5 dark:text-white/5 -rotate-45"></div>
                    </div>

                    <div class="absolute -bottom-5 -end-5">
                        <div class="uil uil-pen lg:text-[150px] text-7xl text-slate-900/5 dark:text-white/5"></div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer Start -->
        <footer class="footer bg-dark-footer relative text-gray-200 dark:text-gray-200">    
            <div class="container relative">
                <div class="grid grid-cols-12">
                    <div class="col-span-12">
                        <div class="py-[60px] px-0">
                            <div class="grid md:grid-cols-12 grid-cols-1 gap-[30px]">
                                <div class="lg:col-span-4 md:col-span-12">
                                    <a href="#" class="text-[22px] focus:outline-none">
                                        <img src="assets/images/Images/logo.png" class="h-16" alt="">
                                    </a>
                                    <p class="mt-6 text-gray-300">Start working with Tailwind CSS that can provide everything you need to generate awareness, drive traffic, connect.</p>
                                    <ul class="list-none mt-6">
                                        <li class="inline"><a href="https://1.envato.market/techwind" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-shopping-cart align-middle" title="Buy Now"></i></a></li>
                                        <li class="inline"><a href="https://dribbble.com/shreethemes" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-dribbble align-middle" title="dribbble"></i></a></li>
                                        <li class="inline"><a href="https://www.behance.net/shreethemes" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-behance" title="Behance"></i></a></li>
                                        <li class="inline"><a href="http://linkedin.com/company/shreethemes" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-linkedin" title="Linkedin"></i></a></li>
                                        <li class="inline"><a href="https://www.facebook.com/shreethemes" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-facebook-f align-middle" title="facebook"></i></a></li>
                                        <li class="inline"><a href="https://www.instagram.com/shreethemes/" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-instagram align-middle" title="instagram"></i></a></li>
                                        <li class="inline"><a href="https://twitter.com/shreethemes" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-twitter align-middle" title="twitter"></i></a></li>
                                        <li class="inline"><a href="mailto:support@shreethemes.in" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-envelope align-middle" title="email"></i></a></li>
                                        <li class="inline"><a href="https://forms.gle/QkTueCikDGqJnbky9" target="_blank" class="h-8 w-8 inline-flex items-center justify-center tracking-wide align-middle duration-500 text-base text-center border border-gray-800 rounded-md hover:border-indigo-600 dark:hover:border-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-600"><i class="uil uil-file align-middle" title="customization"></i></a></li>
                                    </ul><!--end icon-->
                                </div><!--end col-->
                        
                                <div class="lg:col-span-2 md:col-span-4">
                                    <h5 class="tracking-[1px] text-gray-100 font-semibold">Company</h5>
                                    <ul class="list-none footer-list mt-6">
                                        <li><a href="page-aboutus.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> About us</a></li>
                                        <li class="mt-[10px]"><a href="page-services.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Services</a></li>
                                        <li class="mt-[10px]"><a href="page-team.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Team</a></li>
                                        <li class="mt-[10px]"><a href="page-pricing.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Pricing</a></li>
                                        <li class="mt-[10px]"><a href="portfolio-creative-four.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Project</a></li>
                                        <li class="mt-[10px]"><a href="blog.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Blog</a></li>
                                        <li class="mt-[10px]"><a href="auth-login.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Login</a></li>
                                    </ul>
                                </div><!--end col-->
                        
                                <div class="lg:col-span-3 md:col-span-4">
                                    <h5 class="tracking-[1px] text-gray-100 font-semibold">Usefull Links</h5>
                                    <ul class="list-none footer-list mt-6">
                                        <li><a href="page-terms.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Terms of Services</a></li>
                                        <li class="mt-[10px]"><a href="page-privacy.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Privacy Policy</a></li>
                                        <li class="mt-[10px]"><a href="documentation.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Documentation</a></li>
                                        <li class="mt-[10px]"><a href="changelog.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Changelog</a></li>
                                        <li class="mt-[10px]"><a href="widget.html" class="text-gray-300 hover:text-gray-400 duration-500 ease-in-out"><i class="uil uil-angle-right-b"></i> Widget</a></li>
                                    </ul>
                                </div><!--end col-->
    
                                <div class="lg:col-span-3 md:col-span-4">
                                    <h5 class="tracking-[1px] text-gray-100 font-semibold">Newsletter</h5>
                                    <p class="mt-6">Sign up and receive the latest tips via email.</p>
                                    <form>
                                        <div class="grid grid-cols-1">
                                            <div class="my-3">
                                                <label class="form-label">Write your email <span class="text-red-600">*</span></label>
                                                <div class="form-icon relative mt-2">
                                                    <i data-feather="mail" class="w-4 h-4 absolute top-3 start-4"></i>
                                                    <input type="email" class="form-input ps-12 rounded w-full py-2 px-3 h-10 bg-gray-800 border-0 text-gray-100 focus:shadow-none focus:ring-0 placeholder:text-gray-200" placeholder="Email" name="email" required="">
                                                </div>
                                            </div>
                                        
                                            <button type="submit" id="submitsubscribe" name="send" class="py-2 px-5 inline-block font-semibold tracking-wide border align-middle duration-500 text-base text-center bg-indigo-600 hover:bg-indigo-700 border-indigo-600 hover:border-indigo-700 text-white rounded-md">Subscribe</button>
                                        </div>
                                    </form>
                                </div><!--end col-->
                            </div><!--end grid-->
                        </div><!--end col-->
                    </div>
                </div><!--end grid-->
            </div><!--end container-->

            <div class="py-[30px] px-0 border-t border-slate-800">
                <div class="container relative text-center">
                    <div class="grid md:grid-cols-2 items-center">
                        <div class="md:text-start text-center">
                            <p class="mb-0">© <script>document.write(new Date().getFullYear())</script> NAUTI. Design with <i class="mdi mdi-heart text-red-600"></i> by <a href="https://shreethemes.in/" target="_blank" class="text-reset">Cube Master</a>.</p>
                        </div>

                        <ul class="list-none md:text-end text-center mt-6 md:mt-0">
                            <li class="inline"><a href=""><img src="assets/images/payments/american-ex.png" class="max-h-6 inline" title="American Express" alt=""></a></li>
                            <li class="inline"><a href=""><img src="assets/images/payments/discover.png" class="max-h-6 inline" title="Discover" alt=""></a></li>
                            <li class="inline"><a href=""><img src="assets/images/payments/master-card.png" class="max-h-6 inline" title="Master Card" alt=""></a></li>
                            <li class="inline"><a href=""><img src="assets/images/payments/paypal.png" class="max-h-6 inline" title="Paypal" alt=""></a></li>
                            <li class="inline"><a href=""><img src="assets/images/payments/visa.png" class="max-h-6 inline" title="Visa" alt=""></a></li>
                        </ul>
                    </div><!--end grid-->
                </div><!--end container-->
            </div>
        </footer><!--end footer-->
        <!-- Footer End -->



        <!-- Back to top -->
        <a href="#" onclick="topFunction()" id="back-to-top" class="back-to-top fixed hidden text-lg rounded-full z-10 bottom-5 end-5 h-9 w-9 text-center bg-indigo-600 text-white leading-9"><i class="uil uil-arrow-up"></i></a>
        <!-- Back to top -->

       

        <!-- JAVASCRIPTS -->
        <script src="assets/libs/tiny-slider/min/tiny-slider.js"></script>
        <script src="assets/libs/tobii/js/tobii.min.js"></script>
        <script src="assets/libs/feather-icons/feather.min.js"></script>
        <script src="assets/js/plugins.init.js"></script>
        <script src="assets/js/app.js"></script>
        <!-- JAVASCRIPTS -->
    </body>
</html>